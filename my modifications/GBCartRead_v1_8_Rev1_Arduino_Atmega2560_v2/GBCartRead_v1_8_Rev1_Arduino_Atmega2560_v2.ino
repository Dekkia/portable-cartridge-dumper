/*
  GBCartRead-TFT - Atmega2560
  Version: 2.0 (based on 1.8 Rev1)
  Author: Dekkia, based on work from Alex at insideGadgets and Limor Fried/Ladyada at Adafruit Industries
*/

//Define cart-shield pins
// Edit these in pindelcarations.h too
uint8_t latchPin = 10;
uint8_t dataPin = 11;
uint8_t clockPin = 13;
uint8_t rdPin = A5;
uint8_t wrPin = A3;
uint8_t mreqPin = A4;
uint8_t mosfetControlPin = A0;

//Define button-pins:
uint8_t leftButton = 31;
uint8_t rightButton = 27;
uint8_t upButton = 33;
uint8_t downButton = 29;
uint8_t aButton = 23;
uint8_t bButton = 25;

#include "pindeclarations.h"
#include <util/delay.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735
#include <SPI.h>
#include <SD.h>

Sd2Card card;
SdVolume volume;
SdFile root;

//Defin 1.8" TFT-display pins
#define TFT_CS 22 //(CS)
#define TFT_RST 24 //(RST)
#define TFT_DC 26 //(RS)
#define TFT_MOSI 28  // Data out (SDA)
#define TFT_SCLK 30  // Clock out (CLK)
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

int imgDrawY = 0;
int imgDrawX = 0;
int curMenuIndex = 0;
int mainMenuMaxIndex = 2;
int curMenuId = 0;
int lastButtonState = 63;

uint8_t nintendoLogo[] = {0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
                          0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
                          0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E
                         };
char gameTitle[17];
uint16_t cartridgeType = 0;
uint16_t romSize = 0;
uint16_t romBanks = 0;
uint16_t ramSize = 0;
uint16_t ramBanks = 0;
uint16_t ramEndAddress = 0;

void setup() {
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(rdPin, OUTPUT);
  pinMode(wrPin, OUTPUT);
  pinMode(mreqPin, OUTPUT);
  pinMode(mosfetControlPin, OUTPUT);
  PORTF |= (1 << PF0); // Turn mosfet off

  pinMode(leftButton, INPUT_PULLUP);
  pinMode(rightButton, INPUT_PULLUP);
  pinMode(upButton, INPUT_PULLUP);
  pinMode(downButton, INPUT_PULLUP);
  pinMode(aButton, INPUT_PULLUP);
  pinMode(bButton, INPUT_PULLUP);

  // Set pins as inputs
  for (int i = 2; i <= 9; i++) {
    pinMode(i, INPUT);
  }
  rd_wr_mreq_off();

  //Init display:
  tft.initR(INITR_BLACKTAB);
  tft.setTextWrap(true);
  drawWaitScreen("Initializing...");
  Serial.begin(250000);


  Serial.print("Initializing SD card...");
  pinMode(53, OUTPUT);
  if (!SD.begin()) {
    Serial.println("initialization failed.");
    drawSDError();
  } else {
    Serial.println("OK");
    drawMainMenu();
  }
}

void loop() {
  // Wait for serial input
  while (Serial.available() <= 0) {
    delay(5);

    if (PINF & (1 << PF1)) { // Invert mosfet control pin
      PORTF ^= (1 << PF0);

      _delay_ms(50);
      while (PINF & (1 << PF1)); // Wait for button release
      _delay_ms(250);

      // Check if mosfet is on or off (inverted as it's a P mosfet)
      if (PINF & (1 << PF0)) {
        rd_wr_mreq_off(); // Set everying low
        latchPin_low;
        dataPin_low;
        clockPin_low;
      }
      else {
        PORTF &= ~(1 << PF0); // Turn mosfet on
        _delay_ms(250);
      }
    }

    //Check button-state:
    int curState = 0;
    int zero = digitalRead(leftButton);
    int one = digitalRead(rightButton);
    int two = digitalRead(upButton);
    int three = digitalRead(downButton);
    int four =  digitalRead(aButton);
    int five =  digitalRead(bButton);
    bitWrite(curState, 0, zero);
    bitWrite(curState, 1, one);
    bitWrite(curState, 2, two);
    bitWrite(curState, 3, three);
    bitWrite(curState, 4, four);
    bitWrite(curState, 5, five);
    /*
      Codes:
      63 No Input
      47 A
      31 B
      61 Right
      55 Down
      62 Left
      59 Up
    */

    if (lastButtonState != curState) {
      lastButtonState = curState;

      //Serial.println(curState);
      switch (curState) {
        case 55:
          //Down
          curMenuIndex++;
          if (curMenuIndex > mainMenuMaxIndex) {
            curMenuIndex = 0;
          }
          drawMainMenuSelection(curMenuIndex);
          break;
        case 59:
          //Up
          if (curMenuIndex <= 0) {
            curMenuIndex = mainMenuMaxIndex;
          } else {
            curMenuIndex--;
          }
          drawMainMenuSelection(curMenuIndex);
          break;
        case 47:
          //A
          Serial.println(curMenuIndex);
          if (curMenuId == 0) {
            if (curMenuIndex == 0) {
              //HEADER:
              getHeader(true);
            } else if (curMenuIndex == 1) {
              //ROM:
              readRom();
            } else if (curMenuIndex == 2) {
              //RAM READ:
              readRam();
            } else if (curMenuIndex == 3) {
              //RAM WRITE:
            } else if (curMenuIndex == 4) {
              //IMG:
            }
          } else if (curMenuId == -2) {
            drawMainMenu();
          }
          break;
        case 31:
          //B
          if (curMenuId != 0) {
            drawMainMenu();
          }
          break;
      }
    }

  }

  // Decode input
  char readInput[10];
  uint8_t readCount = 0;
  while (Serial.available() > 0) {
    char c = Serial.read();
    readInput[readCount] = c;
    readCount++;

  }
  readInput[readCount] = '\0';

  // Cartridge Header
  if (strstr(readInput, "HEADER")) {
    getHeader(true);
  }
  else if (strstr(readInput, "READROM")) {
    readRom();
  }
  else if (strstr(readInput, "READRAM")) {
    readRam();
  }
  else if (strstr(readInput, "WRITERAM")) {
    //writeRam();
    Serial.println("NOT IMPLEMENTED");
  }
  else if (strstr(readInput, "PAINT")) {
    //drawFirstImageFromCamera();
    Serial.println("NOT IMPLEMENTED");
  }
  rd_wr_mreq_off();
}

//HEADER
void getHeader(bool showUi) {
  rd_wr_mreq_reset();

  // Read cartridge title and check for non-printable text
  for (uint16_t romAddress = 0x0134; romAddress <= 0x143; romAddress++) {
    char headerChar = (char) read_byte(romAddress);
    if ((headerChar >= 0x30 && headerChar <= 0x57) || // 0-9
        (headerChar >= 0x41 && headerChar <= 0x5A) || // A-Z
        (headerChar >= 0x61 && headerChar <= 0x7A)) { // a-z
      gameTitle[(romAddress - 0x0134)] = headerChar;
    }
  }
  gameTitle[16] = '\0';

  // Nintendo Logo Check
  uint8_t logoCheck = 1;
  uint8_t x = 0;
  for (uint16_t romAddress = 0x0104; romAddress <= 0x133; romAddress++) {
    if (nintendoLogo[x] != read_byte(romAddress)) {
      logoCheck = 0;
      break;
    }
    x++;
  }

  cartridgeType = read_byte(0x0147);
  romSize = read_byte(0x0148);
  ramSize = read_byte(0x0149);

  // ROM banks
  romBanks = 2; // Default 32K
  if (romSize >= 1) { // Calculate rom size
    romBanks = 2 << romSize;
  }

  // RAM banks
  ramBanks = 0; // Default 0K RAM
  if (cartridgeType == 6) {
    ramBanks = 1;
  }
  if (ramSize == 2) {
    ramBanks = 1;
  }
  if (ramSize == 3) {
    ramBanks = 4;
  }
  if (ramSize == 4) {
    ramBanks = 16;
  }
  if (ramSize == 5) {
    ramBanks = 8;
  }

  // RAM end address
  if (cartridgeType == 6) {
    ramEndAddress = 0xA1FF;  // MBC2 512bytes (nibbles)
  }
  if (ramSize == 1) {
    ramEndAddress = 0xA7FF;  // 2K RAM
  }
  if (ramSize > 1) {
    ramEndAddress = 0xBFFF;  // 8K RAM
  }

  Serial.println(gameTitle);
  Serial.println(cartridgeType);
  Serial.println(romSize);
  Serial.println(ramSize);
  Serial.println(logoCheck);

  if (showUi) {
    drawHeaderMenu();
  }
}

//READROM
void readRom() {
  drawWaitScreen(" Dumping ROM");
  tft.setTextSize(1);
  getHeader(false);

  String titleString = String(gameTitle);
  if (titleString.length() > 8) {
    titleString = titleString.substring(0, 8);
  }
  String fileName = titleString + ".GB";
  if (titleString.length() <= 0) {
    drawSkippableError("Invalid Game name! Try again.");
    return;
  }
  Serial.println(fileName);
  File romFile = SD.open(fileName, FILE_WRITE);
  if (romFile == false) {
    drawSkippableError("Coudn't create file.");
    return;
  }

  rd_wr_mreq_reset();
  uint16_t romAddress = 0;
  int y = 60;
  int x = 0;
  int colorId = 0;

  // Read number of banks and switch banks
  for (uint16_t bank = 1; bank < romBanks; bank++) {
    if (cartridgeType >= 5) { // MBC2 and above
      write_byte(0x2100, bank); // Set ROM bank
    }
    else { // MBC1
      write_byte(0x6000, 0); // Set ROM Mode
      write_byte(0x4000, bank >> 5); // Set bits 5 & 6 (01100000) of ROM bank
      write_byte(0x2000, bank & 0x1F); // Set bits 0 & 4 (00011111) of ROM bank
    }
    if (bank > 1) {
      romAddress = 0x4000;
    }

    // Read up to 7FFF per bank
    while (romAddress <= 0x7FFF) {
      uint8_t readData[64];
      for (uint8_t i = 0; i < 64; i++) {
        readData[i] = read_byte(romAddress + i);
      }

      romFile.write(readData, 64);
      //Serial.write(readData, 64); // Send the 64 byte chunk
      romAddress += 64;
      switch (colorId) {
        case 0:
          tft.drawPixel(x, y, ST77XX_WHITE);
          break;
        case 1:
          tft.drawPixel(x, y, ST77XX_GREEN);
          break;
        case 2:
          tft.drawPixel(x, y, ST77XX_BLUE);
          break;
      }
      x++;
      if (x >= 128) {
        x  = 0;
        y++;
        if (y >= 160) {
          y = 60;
          colorId++;
          if (colorId >= 3) {
            colorId = 0;
          }
        }
      }

    }
  }
  romFile.close();
  drawMainMenu();
  Serial.println("ROM dump done!");
}

//READRAM
void readRam() {
  drawWaitScreen(" Dumping RAM");
  getHeader(false);

  rd_wr_mreq_reset();

  // MBC2 Fix (unknown why this fixes reading the ram, maybe has to read ROM before RAM?)
  read_byte(0x0134);

  // Does cartridge have RAM
  if (ramEndAddress > 0) {
    int y = 60;
    int x = 0;
    int colorId = 0;

    String titleString = String(gameTitle);
    if (titleString.length() > 8) {
      titleString = titleString.substring(0, 8);
    }
    String fileName = titleString + ".SAV";
    if (titleString.length() <= 0) {
      drawSkippableError("Invalid Game name! Try again.");
      return;
    }
    Serial.println(fileName);
    File romFile = SD.open(fileName, FILE_WRITE);
    if (romFile == false) {
      drawSkippableError("Coudn't create file.");
      return;
    }

    if (cartridgeType <= 4) { // MBC1
      write_byte(0x6000, 1); // Set RAM Mode
    }

    // Initialise MBC
    write_byte(0x0000, 0x0A);

    // Switch RAM banks
    for (uint8_t bank = 0; bank < ramBanks; bank++) {
      write_byte(0x4000, bank);

      // Read RAM
      for (uint16_t ramAddress = 0xA000; ramAddress <= ramEndAddress; ramAddress += 64) {
        uint8_t readData[64];
        for (uint8_t i = 0; i < 64; i++) {
          readData[i] = read_byte(ramAddress + i);
        }

        romFile.write(readData, 64);
        //Serial.write(readData, 64); // Send the 64 byte chunk
        switch (colorId) {
          case 0:
            tft.drawPixel(x, y, ST77XX_WHITE);
            break;
          case 1:
            tft.drawPixel(x, y, ST77XX_GREEN);
            break;
          case 2:
            tft.drawPixel(x, y, ST77XX_BLUE);
            break;
        }
        x++;
        if (x >= 128) {
          x  = 0;
          y++;
          if (y >= 160) {
            y = 60;
            colorId++;
            if (colorId >= 3) {
              colorId = 0;
            }
          }
        }
      }
    }

    // Disable RAM
    write_byte(0x0000, 0x00);

    romFile.close();
    drawMainMenu();
    Serial.println("RAM dump done!");
  } else {
    drawSkippableError("Cartrige has no detectable RAM");
  }

}

//WRITERAM
void writeRam() {

  rd_wr_mreq_reset();

  // MBC2 Fix (unknown why this fixes it, maybe has to read ROM before RAM?)
  read_byte(0x0134);

  // Does cartridge have RAM
  if (ramEndAddress > 0) {
    if (cartridgeType <= 4) { // MBC1
      write_byte(0x6000, 1); // Set RAM Mode
    }

    // Initialise MBC
    write_byte(0x0000, 0x0A);

    // Switch RAM banks
    for (uint8_t bank = 0; bank < ramBanks; bank++) {
      write_byte(0x4000, bank);

      // Write RAM
      for (uint16_t ramAddress = 0xA000; ramAddress <= ramEndAddress; ramAddress++) {
        // Wait for serial input
        while (Serial.available() <= 0);

        // Read input
        uint8_t readValue = (uint8_t) Serial.read();

        // Write to RAM
        mreqPin_low;
        write_byte(ramAddress, readValue);
        asm volatile("nop");
        asm volatile("nop");
        asm volatile("nop");
        mreqPin_high;
      }
    }

    // Disable RAM
    write_byte(0x0000, 0x00);
    Serial.flush(); // Flush any serial data that wasn't processed
  }
}

uint8_t read_byte(uint16_t address) {
  shiftout_address(address); // Shift out address

  mreqPin_low;
  rdPin_low;
  asm volatile("nop"); // Delay a little (minimum is 2 nops, using 3 to be sure)
  asm volatile("nop");
  asm volatile("nop");

  // Read data pins
  byte bval = 0;
  for (int z = 9; z >= 2; z--) {
    if (digitalRead(z) == HIGH) {
      bitWrite(bval, (z - 2), HIGH);
    }
  }

  rdPin_high;
  mreqPin_high;

  return bval;
}

void write_byte(uint16_t address, uint8_t data) {
  // Set pins as outputs
  for (int i = 2; i <= 9; i++) {
    pinMode(i, OUTPUT);
  }

  shiftout_address(address); // Shift out address

  // Read the bits in the received character and turn on the
  // corresponding D0-D7 pins
  for (int z = 9; z >= 2; z--) {
    if (bitRead(data, z - 2) == HIGH) {
      digitalWrite(z, HIGH);
    }
    else {
      digitalWrite(z, LOW);
    }
  }

  // Pulse WR
  wrPin_low;
  asm volatile("nop");
  wrPin_high;

  // Set pins as inputs
  for (int i = 2; i <= 9; i++) {
    pinMode(i, INPUT);
  }
}

// Use the shift registers to shift out the address
void shiftout_address(uint16_t shiftAddress) {
  latchPin_low;
  shiftOut(dataPin, clockPin, MSBFIRST, (shiftAddress >> 8));
  shiftOut(dataPin, clockPin, MSBFIRST, (shiftAddress & 0xFF));
  delayMicroseconds(50);
  latchPin_high;
}

// Turn RD, WR and MREQ to high so they are deselected (reset state)
void rd_wr_mreq_reset(void) {
  rdPin_high; // RD off
  wrPin_high; // WR off
  mreqPin_high; // MREQ off
}

// Turn RD, WR and MREQ off as no power should be applied to GB Cart
void rd_wr_mreq_off(void) {
  rdPin_low;
  wrPin_low;
  mreqPin_low;
}

void drawMainMenu() {
  curMenuId = 0;
  tft.fillScreen(ST77XX_BLACK);
  tft.setCursor(0, 10);
  tft.setTextSize(2);
  tft.setTextColor(ST77XX_WHITE);
  tft.println(" Main Menu");
  tft.setTextSize(1);
  tft.println("   Read Header");
  tft.println("   Dump ROM");
  tft.println("   Dump RAM");
  //tft.println("   Write RAM");
  //tft.println("   View Photos");

  curMenuIndex = 0;
  drawMainMenuSelection(curMenuIndex);
}

void drawMainMenuSelection(int id) {
  if (curMenuId != 0) return;

  //Default config:
  int initPos = 26;

  tft.setTextColor(ST77XX_BLACK);

  //Clear selection below:
  int lastId = id - 1;
  if (id == 0) {
    lastId = mainMenuMaxIndex;
  }
  int pos = initPos + (lastId * 8);
  tft.setCursor(0, pos);
  tft.print(" ->");

  //Clear selection above:
  lastId = id + 1;
  if (id == mainMenuMaxIndex) {
    lastId = 0;
  }
  pos = initPos + (lastId * 8);
  tft.setCursor(0, pos);
  tft.print(" ->");


  //Draw new selection:
  tft.setTextColor(ST77XX_RED);
  pos = initPos + (id * 8);
  tft.setCursor(0, pos);
  tft.print(" ->");
}

void drawHeaderMenu() {
  curMenuId = 1;

  tft.fillScreen(ST77XX_BLACK);
  tft.setCursor(0, 10);
  tft.setTextSize(2);
  tft.setTextColor(ST77XX_WHITE);
  tft.println("Header");
  tft.setTextSize(1);
  tft.println("");

  tft.setTextColor(ST77XX_RED);
  tft.println("Title:");
  tft.setTextColor(ST77XX_WHITE);
  tft.println(gameTitle);

  tft.setTextColor(ST77XX_RED);
  tft.println("Cart Type:");
  tft.setTextColor(ST77XX_WHITE);
  switch (cartridgeType) {
    case 0: tft.println("ROM ONLY"); break;
    case 1: tft.println("MBC1"); break;
    case 2: tft.println("MBC1+RAM"); break;
    case 3: tft.println("MBC1+RAM+BATTERY"); break;
    case 5: tft.println("MBC2"); break;
    case 6: tft.println("MBC2+BATTERY"); break;
    case 8: tft.println("ROM+RAM"); break;
    case 9: tft.println("ROM ONLY"); break;
    case 11: tft.println("MMM01"); break;
    case 12: tft.println("MMM01+RAM"); break;
    case 13: tft.println("MMM01+RAM+BATTERY"); break;
    case 15: tft.println("MBC3+TIMER+BATTERY"); break;
    case 16: tft.println("MBC3+TIMER+RAM+BATTERY"); break;
    case 17: tft.println("MBC3"); break;
    case 18: tft.println("MBC3+RAM"); break;
    case 19: tft.println("MBC3+RAM+BATTERY"); break;
    case 21: tft.println("MBC4"); break;
    case 22: tft.println("MBC4+RAM"); break;
    case 23: tft.println("MBC4+RAM+BATTERY"); break;
    case 25: tft.println("MBC5"); break;
    case 26: tft.println("MBC5+RAM"); break;
    case 27: tft.println("MBC5+RAM+BATTERY"); break;
    case 28: tft.println("MBC5+RUMBLE"); break;
    case 29: tft.println("MBC5+RUMBLE+RAM"); break;
    case 30: tft.println("MBC5+RUMBLE+RAM+BATTERY"); break;
    case 252: tft.println("Gameboy Camera"); break;
    default: tft.println ("Not found");
  }
  // tft.println(cartridgeType);

  tft.setTextColor(ST77XX_RED);
  tft.println("Rom Size:");
  tft.setTextColor(ST77XX_WHITE);
  switch (romSize) {
    case 0: tft.println("32KByte (no ROM banking)"); break;
    case 1: tft.println("64KByte (4 banks)"); break;
    case 2: tft.println("128KByte (8 banks)"); break;
    case 3: tft.println("256KByte (16 banks)"); break;
    case 4: tft.println("512KByte (32 banks)"); break;
    case 5:
      if (cartridgeType == 1 || cartridgeType == 2 || cartridgeType == 3) {
        tft.println("1MByte (63 banks)");
      }
      else {
        tft.println("1MByte (64 banks)");
      }
      break;
    case 6:
      if (cartridgeType == 1 || cartridgeType == 2 || cartridgeType == 3) {
        tft.println("2MByte (125 banks)");
      }
      else {
        tft.println("2MByte (128 banks)");
      }
      break;
    case 7: tft.println("4MByte (256 banks)"); break;
    case 82: tft.println("1.1MByte (72 banks)"); break;
    case 83: tft.println("1.2MByte (80 banks)"); break;
    case 84: tft.println("1.5MByte (96 banks)"); break;
    default: tft.println("Not found");
  }
  //tft.println(romSize);

  tft.setTextColor(ST77XX_RED);
  tft.println("Ram Size:");
  tft.setTextColor(ST77XX_WHITE);

  switch (ramSize) {
    case 0:
      if (cartridgeType == 6) {
        tft.println("512 bytes (nibbles)");
      }
      else {
        tft.println("None");
      }
      break;
    case 1: tft.println("2 KBytes"); break;
    case 2: tft.println("8 KBytes"); break;
    case 3: tft.println("32 KBytes (4 banks of 8Kbytes)"); break;
    case 4: tft.println("128 KBytes (16 banks of 8Kbytes)"); break;
    default: tft.println("Not found");
  }
  //tft.println(ramSize);

}

void drawWaitScreen(String waitReason) {
  curMenuId = -1;
  tft.fillScreen(ST77XX_BLACK);
  tft.setCursor(0, 10);
  tft.setTextSize(2);
  tft.setTextColor(ST77XX_WHITE);
  tft.println("  PLEASE  ");
  tft.println("  WAIT   ");
  tft.setTextSize(1);
  tft.println("");
  tft.println(waitReason);
}

void drawSkippableError(String errorText) {
  curMenuId = -2;
  tft.fillScreen(ST77XX_BLACK);
  tft.setCursor(0, 10);
  tft.setTextSize(2);
  tft.setTextColor(ST77XX_WHITE);
  tft.println("  ERROR  ");
  tft.setTextSize(1);
  tft.println("");
  tft.println(errorText);
  tft.println("");
  tft.println("Press A to continue.");
}

void drawSDError() {
  curMenuId = -1;
  tft.fillScreen(ST77XX_BLACK);
  tft.setCursor(0, 10);
  tft.setTextSize(2);
  tft.setTextColor(ST77XX_RED);
  tft.println("SD ERROR");
  tft.setTextSize(1);
  tft.setTextColor(ST77XX_WHITE);
  tft.println("");
  tft.println("Check if SD Card is inserted and correctly formatted!");
}
/*
  void drawFirstImageFromCamera() {
  Serial.println(cartridgeType);

  rd_wr_mreq_reset();
  tft.fillScreen(ST77XX_BLACK);
  // MBC2 Fix (unknown why this fixes reading the ram, maybe has to read ROM before RAM?)
  read_byte(0x0134);

  if (cartridgeType <= 4) { // MBC1
    write_byte(0x6000, 1); // Set RAM Mode
  }

  // Initialise MBC
  write_byte(0x0000, 0x0A);

  //uint8_t startLocation = 0x2000 + 0xA000;
  int imgDrawY = 0;
  int imgDrawX = 0;

  // Switch RAM banks
  for (uint8_t bank = 0; bank < ramBanks; bank++) {
    write_byte(0x4000, bank);

    // Read RAM
    for (uint16_t ramAddress = 0xA000; ramAddress <= ramEndAddress; ramAddress++) {
      uint8_t curByte = read_byte(ramAddress);
      parseAndDrawPixel(curByte, 6);
      parseAndDrawPixel(curByte, 4);
      parseAndDrawPixel(curByte, 2);
      parseAndDrawPixel(curByte, 0);
    }
  }
  // Disable RAM
  write_byte(0x0000, 0x00);
  }

  void parseAndDrawPixel(int src, int cnt) {
  int useColor = 0;
  int p1 = bitRead(src, cnt + 1);
  int p2 = bitRead(src, cnt);

  if (p1 == 0) {
    if (p2 == 0) {
      //00
      useColor = 0;
    } else {
      //01
      useColor = 1;
    }
  } else {
    if (p2 == 0) {
      //10
      useColor = 2;
    } else {
      //11
      useColor = 3;
    }
  }

  drawImagePixel(imgDrawX, imgDrawY, useColor);

  imgDrawX++;
  if (imgDrawX >= 128) {
    imgDrawX = 0;
    imgDrawY++;
  }
  if (imgDrawY > 160) {
    imgDrawY = 0;
  }
  }

  void drawImagePixel(int x, int y, int color) {
  if (color == 0) {
    tft.drawPixel(x, y, ST77XX_WHITE);
  } else if (color == 1) {
    tft.drawPixel(x, y, ST77XX_GREEN);
  } else if (color == 2) {
    tft.drawPixel(x, y, ST77XX_BLUE);
  } else if (color == 3) {
    tft.drawPixel(x, y, ST77XX_BLACK);
  }
  }
*/
